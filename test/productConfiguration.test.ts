import { describe, expect, test } from "vitest";
import { isProductConfigurationValid } from "../src/utils/productConfiguration";

describe("ProductConfiguration", () => {
  test("returns false if the selection match with excludes", () => {
    const chosenOptions = {
      printingmethod: "offset",
      material: "135gr_mat_mc",
    };

    const excludes = [
      [
        {
          options: ["135gr_mat_mc"],
          property: "material",
        },
        {
          options: ["offset"],
          property: "printingmethod",
        },
      ],
    ];

    const result = isProductConfigurationValid(chosenOptions, excludes);
    expect(result.isValid).toBe(false);
  });


  test("returns false if the selection match with excludes and have more properties", () => {
    const chosenOptions = {
      printingmethod: "offset",
      material: "135gr_mat_mc",
      size: "A4",
    };

    const excludes = [
      [
        {
          options: ["135gr_mat_mc"],
          property: "material",
        },
        {
          options: ["offset"],
          property: "printingmethod",
        },
      ],
    ];

    const result = isProductConfigurationValid(chosenOptions, excludes);
    expect(result.isValid).toBe(false);
  });

  test("returns true if the selection doesn't match with excludes", () => {
    const chosenOptions = {
      printingmethod: "offset",
      material: "135gr_mat_mc",
      size: "A4",
    };

    const excludes = [
      [
        {
          options: ["135gr_mat_mc"],
          property: "material",
        },
        {
          options: ["digital"],
          property: "printingmethod",
        },
      ],
    ];

    const result = isProductConfigurationValid(chosenOptions, excludes);
    expect(result.isValid).toBe(true);
  });
});
