export const routes = [
    {
        path: '/',
        name: 'products',
        component: () => import('./views/Products.vue')
    },
    {
        path: '/products/posters',
        name: 'posters',
        component: () => import('./views/Posters.vue')
    },
    {
        path: '/products/flyers',
        name: 'flyers',
        component: () => import('./views/Flyers.vue')
    },
    {
        path: '/products/businesscards',
        name: 'businesscards',
        component: () => import('./views/Businesscards.vue')
    }
]