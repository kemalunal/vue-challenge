import { createApp } from 'vue'
import App from './App.vue'
import './style.css'
import 'primevue/resources/themes/saga-blue/theme.css';
import 'primevue/resources/primevue.min.css';


import { createRouter, createWebHistory } from 'vue-router'
import { routes } from './routes'
import PrimeVue from 'primevue/config';
import ToastService from 'primevue/toastservice';


const app = createApp(App);
app.use(PrimeVue);
app.use(ToastService);

const router : any = createRouter({history: createWebHistory(), routes: routes});
app.use(router);
app.mount('#app')
