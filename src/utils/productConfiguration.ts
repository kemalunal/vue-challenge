import {
  ProductConfiguration,
  ExcludeItem,
  MatchedOption,
  ValidationResult,
} from "../types/types";

export const isProductConfigurationValid = (
  chosenOptions: ProductConfiguration,
  exclusionRules: ExcludeItem[]
): ValidationResult => {
  let matchedSelections: MatchedOption[] = [];
  const selectedProperties = Object.keys(chosenOptions);

  const isValid = exclusionRules.every((excludeGroup) => {
    const excludedProperties = excludeGroup.map((item) => item.property);
    const ruleMatchesProperties = excludedProperties.every((property) =>
      selectedProperties.includes(property)
    );

    if (!ruleMatchesProperties) {
      return true;
    }

    const _matchedSelections: MatchedOption[] = [];
    const ruleMatchesOptions = excludedProperties.every((property) => {
      const excludedOptions = excludeGroup.find(
        (item) => item.property === property
      )?.options;
      return excludedOptions?.some((option) => {
        if (chosenOptions[property] === option) {
          _matchedSelections.push({ property, option });
          return true;
        }
        return false;
      });
    });

    if (ruleMatchesOptions) {
      matchedSelections.push(..._matchedSelections);
    }

    //if it is match the exclude options with selection, it is not valid
    return !ruleMatchesOptions;
  });


  return {
    isValid,
    matchedSelections,
  };
};
