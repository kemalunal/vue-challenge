export type ProductData = {
  sku: string;
  title: string;
  properties: Property[];
  excludes: ExcludeItem[];
}

export type Property ={
  slug: string;
  title: string;
  locked: boolean;
  options: Option[];
  viewMode?: ViewMode;
  type?: Type;
  optionsInSummary?: string[];
  parentOptions?: ParentOption[];
}

export type Option = {
  slug: number | string;
  name?: null | string;
  nullable: boolean;
  width?: number | string;
  height?: number | string;
  parent?: string;
  customSizes?: CustomSizes;
  eco?: boolean;
  pages?: Page[];
  enrichments?: Enrichment[];
  type?: string;
  description?: string;
}

export type CustomSizes = {
  minHeight: number;
  sizeUnit: string;
  minWidth: number;
  maxHeight: number;
  maxWidth: number;
}

export type Enrichment = {
  propertySlug: PropertySlug;
  optionSlug: string;
}

export enum PropertySlug {
  Color = "color",
  Thickening = "thickening",
  Thickness = "thickness",
  Weight = "weight",
}

export type Page = {
  numberOfColors: number;
  colorspace: Colorspace;
}

export enum Colorspace {
  Cmyk = "cmyk",
  Indexed = "indexed",
}

export type ParentOption = {
  slug: string;
  name: string;
  nullable: boolean;
  width?: number;
  height?: number;
  eco?: boolean;
  enrichments?: any[];
}

export enum Type {
  Checkbox = "checkbox",
  Radio = "radio",
}

export type ViewMode = {
  reseller: Reseller;
  storefront?: Reseller;
}

export enum Reseller {
  Default = "default",
  Dropdown = "dropdown",
  SummaryModal = "summary-modal",
}

export type ProductConfiguration = {
  [key: string]: string;
};

export type ExludeOption = {
  property: string;
  options: (string | number)[];
};

export type ExcludeItem = ExludeOption[];


export type MatchedOption = {
  property: string;
  option: string | number;
}

export type ValidationResult = {
  isValid: boolean;
  matchedSelections?: MatchedOption[];
};